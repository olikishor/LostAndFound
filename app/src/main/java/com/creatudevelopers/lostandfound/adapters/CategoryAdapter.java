package com.creatudevelopers.lostandfound.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.creatudevelopers.lostandfound.R;
import com.creatudevelopers.lostandfound.models.PlaylistObject;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MylistViewHolder> {

    private static final String TAG = CategoryAdapter.class.getSimpleName();

    private Context context;
    private List<PlaylistObject> playlists;

    public CategoryAdapter(Context context, List<PlaylistObject> playlists) {
        this.context = context;
        this.playlists = playlists;
    }

    @Override
    public MylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_fragment_single_card, parent, false);
        return new MylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MylistViewHolder holder, int position) {
        PlaylistObject playlistObject = playlists.get(position);
        holder.playlistTitle.setText(playlistObject.getPlaylistTitle());
        holder.playlistTracks.setText(playlistObject.getPlaylistTracks());

    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public class MylistViewHolder extends RecyclerView.ViewHolder {

        public TextView playlistTitle;
        public TextView playlistTracks;
        public ImageView playlistCover;

        public MylistViewHolder(View itemView) {
            super(itemView);

            playlistTitle = (TextView) itemView.findViewById(R.id.name);
            playlistTracks = (TextView) itemView.findViewById(R.id.address);
            playlistCover = (ImageView) itemView.findViewById(R.id.image);
        }
    }

}
