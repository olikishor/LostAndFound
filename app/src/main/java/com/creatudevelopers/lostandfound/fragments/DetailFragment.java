package com.creatudevelopers.lostandfound.fragments;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creatudevelopers.lostandfound.R;
import com.creatudevelopers.lostandfound.adapters.CategoryAdapter;
import com.creatudevelopers.lostandfound.helpers.DisableableAppBarLayoutBehavior;
import com.creatudevelopers.lostandfound.models.PlaylistObject;

import java.util.ArrayList;
import java.util.List;


public class DetailFragment extends Fragment {

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewActivity = inflater.inflate(R.layout.activity_main, container, false);
        AppBarLayout  appBarLayout = (AppBarLayout) viewActivity.findViewById(R.id.homeAppbar);
        Toolbar  toolbar = (Toolbar) viewActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.GONE);
        //CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        //((DisableableAppBarLayoutBehavior) layoutParams.getBehavior()).setEnabled(false);
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        return view;
    }

}
